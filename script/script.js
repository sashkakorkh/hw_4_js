/*Exercise 1*/
/*1. Функції потрібні для того, щоб не дублючи код,
робити потрібну нам дію, записану у функцію, в будь-якому місці коду, де функцію можна викликати
2. Щоб не захаращувати код глобальними змінними, ми передаємо  до функції аргументи, з якими вона вже працює в середині
3. return потрібен щоб передати назад результат роботи функції. Якщо ми самостійно не прописуємо, що повертати,
то функція повертає underfined, не дивлячись на те що виконала якісь дії*/


/*Exercise 2*/

let firstNumber = prompt('Enter number first number');


while (!firstNumber || isNaN(firstNumber) || firstNumber === 'null' || (!/\S/.test(firstNumber))) {
    firstNumber = prompt('Error. Enter first number one more time', `You've entered ${firstNumber}`);
}

let secondNumber = prompt('Enter number second number');

while (!secondNumber || isNaN(secondNumber) || secondNumber === 'null' || (!/\S/.test(secondNumber))) {
    secondNumber = prompt('Error. Enter number second number one more time', `You've entered ${secondNumber}`);
}

let operation = prompt('Enter what to do with numbers', '+ or - or * or /')

while (!operation || !isNaN(operation) || (operation !== '+' && operation !== '-'&& operation !== '*' && operation !== '/') || operation === 'null' || (!/\S/.test(operation))){
    secondNumber = prompt('Error. Enter what to do with numbers one more time', `You've entered ${operation}`);
}


function operateWithNumbers (firstNum, secondNum, userOperation) {
    switch (userOperation) {
        case '+' :
        return +firstNum + +secondNum;
        case'-' :
        return +firstNum - +secondNum;
        case '*' :
        return +firstNum * +secondNum;
        case '/' :
            if(+secondNum === 0) {
                throw new Error('Can\'t divide by 0');
            }
        return +firstNum / +secondNum;
    }
}

console.log(operateWithNumbers(firstNumber,secondNumber,operation))




